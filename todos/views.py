from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from todos.models import TodoItem, TodoList


# Create your views here.
class TodoListListView(ListView):
    model = TodoList
    template_name = "todo_list/list.html"


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todo_list/detail.html"


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todo_list/create.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", kwargs={"pk": self.object.pk})


class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todo_list/update.html"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", kwargs={"pk": self.object.pk})


class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todo_list/delete.html"
    success_url = reverse_lazy("todo_list_list")


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todo_item/create.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy(
            "todo_list_detail", kwargs={"pk": self.object.list.pk}
        )


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todo_item/update.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy(
            "todo_list_detail", kwargs={"pk": self.object.list.pk}
        )
