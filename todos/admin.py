from django.contrib import admin
from todos.models import TodoList, TodoItem

# Register your models here.
class TodoListAdmin(admin.ModelAdmin):
    pass


admin.site.register(TodoList, TodoListAdmin)


class TodoItemAdmin(admin.ModelAdmin):
    pass


admin.site.register(TodoItem, TodoItemAdmin)
