from django.urls import path

from todos.views import (
    TodoItemCreateView,
    TodoItemUpdateView,
)

urlpatterns = [
    path("create/", TodoItemCreateView.as_view(), name="todo_item_create"),
    path(
        "<int:pk>/edit/", TodoItemUpdateView.as_view(), name="todo_item_update"
    ),
]
