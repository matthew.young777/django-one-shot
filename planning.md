#Planning

* [x] Fork and clone
* [x] create new virtual environment
  * [x] `python -m venv .venv`
* [x] Activate the virtual environment
  * [x] `.venv\Scripts\Activate.ps1`
* [x] Upgrade Pip
  * [x] `python -m pip install --upgrade pip`
* [x] Install Django
  * [x] `pip install Django`
* [x] Install black
  * [x] `pip install black`
* [x] Install flake8
  * [x] `pip install flake8`
* [x] Install djlint
  * [x] `pip install djlint`
* [x] Install django debug toolbar
  * [x] `pip install django-debug-toolbar`
* [ ] 